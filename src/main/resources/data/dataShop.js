const shops = [
    {
        cod: "shop-1",
        name: "shop-name-1",
        email: "shop-1@domain.com",
        address: "Portazgo, 1",
    },
    {
        cod: "shop-2",
        name: "shop-name-2",
        email: "shop-2@domain.com",
        address: "Portazgo, 2",
    },
    {
        cod: "shop-3",
        name: "shop-name-3",
        email: "shop-3@domain.com",
        address: "Portazgo, 3",
    },
    {
        cod: "shop-4",
        name: "shop-name-4",
        email: "shop-4@domain.com",
        address: "Portazgo, 4",
    }
];

db.shopDocument.insertMany(shops);
