package com.daxamayac.shop.domain.script;

import reactor.core.publisher.Mono;

public interface ScriptService {

    Mono<Void> loadDataFromScript();
}
