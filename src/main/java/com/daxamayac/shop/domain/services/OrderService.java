package com.daxamayac.shop.domain.services;

import com.daxamayac.shop.domain.model.Order;
import com.daxamayac.shop.domain.persistence.CustomerPersistence;
import com.daxamayac.shop.domain.persistence.OrderPersistence;
import com.daxamayac.shop.domain.persistence.ShopPersistence;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

@Service
@Slf4j
@RequiredArgsConstructor
public class OrderService {

    private final OrderPersistence orderPersistence;
    private final CustomerPersistence customerPersistence;
    private final ShopPersistence shopPersistence;

    public Mono<Order> create(Order order) {
        return this.customerPersistence.assertCustomerExist(order.getUuid())
                .then(this.shopPersistence.assertShopAndProductExists(order.getOrderLines()))
                .then(Mono.just(order))
                .flatMap(this.orderPersistence::create);
        //TODO .doOnSuccess(); save transaccion per shop
    }

}
