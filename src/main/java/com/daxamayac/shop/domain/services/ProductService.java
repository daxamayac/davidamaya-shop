package com.daxamayac.shop.domain.services;

import com.daxamayac.shop.domain.exceptions.BadRequestException;
import com.daxamayac.shop.domain.model.Product;
import com.daxamayac.shop.domain.persistence.ProductPersistence;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.Objects;

@Service
@Slf4j
@RequiredArgsConstructor
public class ProductService {

    private final ProductPersistence productPersistence;

    public Flux<Product> saveAll(Flux<Product> products) {
        return this.productPersistence.saveAll(products);
    }

    public Flux<Product> findAll() {
        return this.productPersistence.findAll();
    }

    public Mono<Product> updateStock(String cod, Integer stock) {
        return this.stockIsPositiveNonZero(stock)
                .then(this.productPersistence.updateStock(cod, stock));
    }

    private Mono<Void> stockIsPositiveNonZero(Integer stock) {
        if (Objects.isNull(stock) || stock <= 0) {
            return Mono.error(new BadRequestException("Stock has to be greater than zero"));
        }
        return Mono.empty();
    }
}
