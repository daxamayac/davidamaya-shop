package com.daxamayac.shop.domain.services;

import com.daxamayac.shop.domain.mock.MockProductService;
import com.daxamayac.shop.domain.model.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Flux;

@Component
public class SeederFromMockService {

    private final MockProductService mockProductService;
    private final ProductService productService;

    @Autowired
    public SeederFromMockService(MockProductService mockProductService, ProductService productService) {
        this.mockProductService = mockProductService;
        this.productService = productService;
        init();
    }

    private void init() {
        Flux<Product> products = this.mockProductService.findAllProducts();
        this.productService.saveAll(products).blockLast();
    }

}
