package com.daxamayac.shop.domain.services;

import com.daxamayac.shop.domain.model.Customer;
import com.daxamayac.shop.domain.persistence.CustomerPersistence;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Service
@Slf4j
@RequiredArgsConstructor
public class CustomerService {

    private final CustomerPersistence customerPersistence;

    public Mono<Customer> create(Customer customer) {
        return this.customerPersistence.create(customer);
    }

    public Flux<Customer> findAll() {
        return this.customerPersistence.findAll();
    }

    public Mono<Void> delete(String uuid) {
        return this.customerPersistence.delete(uuid);
    }

    public Mono<Customer> update(String uuid, Customer customer) {
        return this.customerPersistence.update(uuid, customer);
    }
}
