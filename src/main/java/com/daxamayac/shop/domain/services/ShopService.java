package com.daxamayac.shop.domain.services;

import com.daxamayac.shop.domain.model.Shop;
import com.daxamayac.shop.domain.persistence.ShopPersistence;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

@Service
@Slf4j
@RequiredArgsConstructor
public class ShopService {

    private final ShopPersistence shopPersistence;

    public Mono<Shop> addProduct(String cod, String productCod) {
        return this.shopPersistence.addProduct(cod, productCod);
    }

    public Mono<Shop> removeProduct(String cod, String productCod) {
        return this.shopPersistence.removeProduct(cod, productCod);
    }
}
