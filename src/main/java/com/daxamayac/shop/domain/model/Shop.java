package com.daxamayac.shop.domain.model;

import lombok.Data;
import lombok.Singular;

import java.util.List;

@Data
public class Shop {
    private String id;
    private String cod;
    private String name;
    private String address;
    private String email;

    @Singular("product")
    private List<Product> products;

}
