package com.daxamayac.shop.domain.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;


@Data
@NoArgsConstructor
@Builder
@AllArgsConstructor
public class OrderLine {
    @NotBlank
    private String productCod;
    @NotBlank
    private String shopCod;
    @NotNull
    private String description;
    @NotNull
    private Integer amount;
    private BigDecimal price;


}