package com.daxamayac.shop.domain.model;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class Product {
    private String id;
    private String cod;
    private String name;
    private BigDecimal price;
    private Integer stock;

}
