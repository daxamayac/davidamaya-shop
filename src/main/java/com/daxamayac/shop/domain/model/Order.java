package com.daxamayac.shop.domain.model;

import com.daxamayac.shop.domain.util.UUIDBase64;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;

@Data
@NoArgsConstructor
@Builder
@AllArgsConstructor
public class Order {
    private String uuid;
    private String description;
    @NotBlank
    private String userUuid;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime date;

    private List<OrderLine> orderLines;

    public void doDefault() {
        if (Objects.isNull(uuid)) {
            this.uuid = UUIDBase64.URL.encode();
        }
        if (Objects.isNull(date)) {
            this.date = LocalDateTime.now();
        }
    }
}