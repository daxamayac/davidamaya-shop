package com.daxamayac.shop.domain.persistence;

import com.daxamayac.shop.domain.model.OrderLine;
import com.daxamayac.shop.domain.model.Shop;
import reactor.core.publisher.Mono;

import java.util.List;

public interface ShopPersistence {

    Mono<Shop> addProduct(String cod, String productCod);

    Mono<Shop> removeProduct(String cod, String productCod);

    Mono<Void> assertShopAndProductExists(List<OrderLine> orderLines);
}
