package com.daxamayac.shop.domain.persistence;

import com.daxamayac.shop.domain.model.Product;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface ProductPersistence {

    Flux<Product> saveAll(Flux<Product> products);

    Flux<Product> findAll();

    Mono<Product> updateStock(String cod, Integer stock);

    Mono<Product> readAndWriteStockByCod(String cod, Integer stockIncrement);
}
