package com.daxamayac.shop.domain.persistence;

import com.daxamayac.shop.domain.model.Order;
import reactor.core.publisher.Mono;

public interface OrderPersistence {


    Mono<Order> create(Order order);
}
