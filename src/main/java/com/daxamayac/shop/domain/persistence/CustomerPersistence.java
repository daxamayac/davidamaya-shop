package com.daxamayac.shop.domain.persistence;

import com.daxamayac.shop.domain.model.Customer;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface CustomerPersistence {
    Mono<Customer> create(Customer customer);

    Flux<Customer> findAll();

    Mono<Void> delete(String uuid);

    Mono<Customer> update(String uuid, Customer customer);

    Mono<Void> assertCustomerExist(String uuid);
}
