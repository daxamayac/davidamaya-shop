package com.daxamayac.shop.domain.mock;

import com.daxamayac.shop.domain.model.Product;
import reactor.core.publisher.Flux;

public interface MockProductService {

    Flux<Product> findAllProducts();
}
