package com.daxamayac.shop.domain.mock;

import reactor.core.publisher.Mono;

public interface MockStockService {

    Mono<Integer> requestStock10();

    Mono<Integer> requestStock5();
}
