package com.daxamayac.shop.application.rest.endpoints;

import com.daxamayac.shop.domain.model.Order;
import com.daxamayac.shop.domain.services.OrderService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

import javax.validation.Valid;

@RestController
@RequestMapping(OrderEndpoint.ORDERS)
@RequiredArgsConstructor
public class OrderEndpoint {

    public static final String ORDERS = "/orders";

    private final OrderService orderService;

    @PostMapping
    public Mono<Order> create(@Valid @RequestBody Order order) {
        order.doDefault();
        return this.orderService.create(order);
    }


}
