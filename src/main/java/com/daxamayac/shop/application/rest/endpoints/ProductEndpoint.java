package com.daxamayac.shop.application.rest.endpoints;

import com.daxamayac.shop.domain.model.Product;
import com.daxamayac.shop.domain.services.ProductService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping(ProductEndpoint.PRODUCTS)
@RequiredArgsConstructor
public class ProductEndpoint {

    public static final String PRODUCTS = "/products";
    public static final String COD = "/{cod}";
    private final ProductService productService;

    @GetMapping
    public Flux<Product> findAll(@RequestParam(required = false) String fields) {
        return productService.findAll();
    }

    @PatchMapping(COD)
    public Mono<Product> updateStock(@PathVariable String cod, @RequestBody Integer stock) {
        return this.productService.updateStock(cod, stock);
    }

}
