package com.daxamayac.shop.application.rest.endpoints;

import com.daxamayac.shop.domain.model.Customer;
import com.daxamayac.shop.domain.services.CustomerService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import javax.validation.Valid;

@RestController
@RequestMapping(CustomerEndpoint.CUSTOMERS)
@RequiredArgsConstructor
public class CustomerEndpoint {

    public static final String CUSTOMERS = "/customers";
    public static final String UUID = "/{uuid}";
    private final CustomerService customerService;

    @PostMapping
    public Mono<Customer> create(@Valid @RequestBody Customer customer) {
        return customerService.create(customer);
    }

    @GetMapping
    public Flux<Customer> findAll() {
        return customerService.findAll();
    }

    @PutMapping(UUID)
    public Mono<Customer> update(@PathVariable String uuid, @Valid @RequestBody Customer customer) {
        return customerService.update(uuid, customer);
    }


    @DeleteMapping(UUID)
    public Mono<Void> delete(@PathVariable String uuid) {
        return customerService.delete(uuid);
    }

}
