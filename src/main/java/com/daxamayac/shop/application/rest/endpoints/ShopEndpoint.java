package com.daxamayac.shop.application.rest.endpoints;

import com.daxamayac.shop.domain.model.Shop;
import com.daxamayac.shop.domain.services.ShopService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping(ShopEndpoint.SHOPS)
@RequiredArgsConstructor
public class ShopEndpoint {

    public static final String SHOPS = "/shops";
    public static final String COD = "/{cod}";
    public static final String PRODUCTS = "/products";
    public static final String ADD = "/append";
    public static final String REMOVE = "/remove";

    private final ShopService shopService;

    @PatchMapping(COD + PRODUCTS + ADD)
    public Mono<Shop> addProduct(@PathVariable String cod, @RequestBody String productCod) {
        return this.shopService.addProduct(cod, productCod);
    }

    @PatchMapping(COD + PRODUCTS + REMOVE)
    public Mono<Shop> removeProduct(@PathVariable String cod, @RequestBody String productCod) {
        return this.shopService.removeProduct(cod, productCod);
    }

}
