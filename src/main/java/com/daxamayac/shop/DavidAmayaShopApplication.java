package com.daxamayac.shop;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DavidAmayaShopApplication {

    public static void main(String[] args) {
        SpringApplication.run(DavidAmayaShopApplication.class, args);
    }

}
