package com.daxamayac.shop.infrastructure.mock.dtos;


public record MockStockDto(Integer stock) {

}
