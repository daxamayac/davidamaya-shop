package com.daxamayac.shop.infrastructure.mock.dtos;


import com.daxamayac.shop.domain.model.Product;

import java.util.List;


public record MockProductsDto(List<Product> prods) {

}
