package com.daxamayac.shop.infrastructure.mock;

import com.daxamayac.shop.domain.mock.MockProductService;
import com.daxamayac.shop.domain.model.Product;
import com.daxamayac.shop.infrastructure.mock.dtos.MockProductsDto;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Flux;

@Service
@Slf4j
public class MockProductServiceRest implements MockProductService {

    private final String mockProductsUrl;
    private final WebClient.Builder webClientBuilder;

    @Autowired
    public MockProductServiceRest(
            @Value("${daxamayac.mock.products}") String mockProductsUrl,
            WebClient.Builder webClientBuilder
    ) {
        this.mockProductsUrl = mockProductsUrl;
        this.webClientBuilder = webClientBuilder;
    }

    @Override
    public Flux<Product> findAllProducts() {
        return this.webClientBuilder.build()
                .get()
                .uri(this.mockProductsUrl)
                .exchangeToMono(clientResponse -> clientResponse.bodyToMono(MockProductsDto.class))
                .flatMapIterable(MockProductsDto::prods);
    }
}
