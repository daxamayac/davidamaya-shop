package com.daxamayac.shop.infrastructure.mock;

import com.daxamayac.shop.domain.mock.MockStockService;
import com.daxamayac.shop.infrastructure.mock.dtos.MockStockDto;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

@Service
@Slf4j
public class MockStockServiceRest implements MockStockService {

    private final String mockStock10Url;
    private final String mockStock5Url;
    private final WebClient.Builder webClientBuilder;

    @Autowired
    public MockStockServiceRest(
            @Value("${daxamayac.mock.stock-10}") String mockStock10Url,
            @Value("${daxamayac.mock.stock-5}") String mockStock5Url,
            WebClient.Builder webClientBuilder
    ) {
        this.mockStock10Url = mockStock10Url;
        this.mockStock5Url = mockStock5Url;
        this.webClientBuilder = webClientBuilder;
    }

    @Override
    public Mono<Integer> requestStock10() {
        return this.webClientBuilder.build()
                .get()
                .uri(this.mockStock10Url)
                .exchangeToMono(clientResponse -> clientResponse.bodyToMono(MockStockDto.class))
                .map(MockStockDto::stock);
    }

    @Override
    public Mono<Integer> requestStock5() {
        return this.webClientBuilder.build()
                .get()
                .uri(this.mockStock5Url)
                .exchangeToMono(clientResponse -> clientResponse.bodyToMono(MockStockDto.class))
                .map(MockStockDto::stock);
    }
}
