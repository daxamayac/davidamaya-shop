package com.daxamayac.shop.infrastructure.script;

import com.daxamayac.shop.domain.script.ScriptService;
import com.daxamayac.shop.domain.util.LoadResource;
import lombok.extern.slf4j.Slf4j;
import org.bson.Document;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.ScriptOperations;
import org.springframework.data.mongodb.core.script.ExecutableMongoScript;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

import java.util.Arrays;
import java.util.Objects;

@Service
@Slf4j
public class ScriptServiceMongoDb implements ScriptService {

    private final MongoTemplate mongoTemplate;
    private final String[] scripts;

    @Autowired
    public ScriptServiceMongoDb(MongoTemplate mongoTemplate, @Value("${daxamayac.data.init}") String[] scripts) {
        this.mongoTemplate = mongoTemplate;
        this.scripts = scripts;
        loadDataFromScript().block();
    }


    public Mono<Void> loadDataFromScript() {
        Arrays.stream(scripts).forEach(scriptPath ->
        {
            String script = LoadResource.readFileToString(scriptPath);

            boolean isSuccess = this.executeScriptMongo(script);
            if (isSuccess)
                log.debug("Script successfully executed: " + scriptPath);
        });

        return Mono.empty();
    }

    private boolean executeScriptMongo(String script) {
        boolean resp = false;

        ScriptOperations scriptOps = mongoTemplate.scriptOps();
        ExecutableMongoScript echoScript = new ExecutableMongoScript(script);
        Object scriptResult = scriptOps.execute(echoScript);

        Document document = (Document) scriptResult;
        if (Objects.nonNull(document))
            resp = document.getBoolean("acknowledged");

        return resp;
    }
}
