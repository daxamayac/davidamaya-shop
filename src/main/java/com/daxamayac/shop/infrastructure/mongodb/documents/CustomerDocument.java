package com.daxamayac.shop.infrastructure.mongodb.documents;

import com.daxamayac.shop.domain.model.Customer;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.beans.BeanUtils;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@NoArgsConstructor
@Document
public class CustomerDocument {
    @Id
    private String id;
    @Indexed(unique = true)
    private String uuid;
    private String name;

    public CustomerDocument(Customer customer) {
        BeanUtils.copyProperties(customer, this);
    }

    public Customer toCustomer() {
        Customer customer = new Customer();
        BeanUtils.copyProperties(this, customer);
        return customer;
    }
}
