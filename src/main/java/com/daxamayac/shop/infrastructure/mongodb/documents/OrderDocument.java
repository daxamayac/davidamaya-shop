package com.daxamayac.shop.infrastructure.mongodb.documents;

import com.daxamayac.shop.domain.model.Order;
import com.daxamayac.shop.domain.model.OrderLine;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.beans.BeanUtils;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Data
@NoArgsConstructor
@Document
public class OrderDocument {
    @Id
    private String id;
    @Indexed(unique = true)
    private String uuid;
    private String description;
    private String userUuid;
    private LocalDateTime date;

    private List<OrderLine> orderLines;

    public OrderDocument(Order order) {
        BeanUtils.copyProperties(order, this);
        this.orderLines = new ArrayList<>();
    }

    public void add(OrderLine orderLine) {
        this.orderLines.add(orderLine);
    }

    public Order toOrder() {
        Order order = new Order();
        BeanUtils.copyProperties(this, order);

//        if (Objects.nonNull(this.getOrderLines())) {
//            List<OrderLine> orderLines = new ArrayList<>();
//            for (int i = 0; i < this.getOrderLineEntities().size(); i++) {
//                orderLines.add(this.getOrderLineEntities().get(i).toOrderLine());
//            }
//            order.setOrderLines(orderLines);
//        }
        return order;
    }
}