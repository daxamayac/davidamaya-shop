package com.daxamayac.shop.infrastructure.mongodb.repository;

import com.daxamayac.shop.infrastructure.mongodb.documents.CustomerDocument;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import reactor.core.publisher.Mono;

public interface CustomerRepository extends ReactiveCrudRepository<CustomerDocument, String> {

    Mono<Void> deleteByUuid(String uuid);

    Mono<CustomerDocument> findByUuid(String uuid);

    Mono<Boolean> existsByUuid(String uuid);
}
