package com.daxamayac.shop.infrastructure.mongodb.persistence;

import com.daxamayac.shop.domain.exceptions.ConflictException;
import com.daxamayac.shop.domain.exceptions.NotFoundException;
import com.daxamayac.shop.domain.mock.MockStockService;
import com.daxamayac.shop.domain.model.Product;
import com.daxamayac.shop.domain.persistence.ProductPersistence;
import com.daxamayac.shop.infrastructure.mongodb.documents.ProductDocument;
import com.daxamayac.shop.infrastructure.mongodb.repository.ProductRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.mongodb.core.FindAndModifyOptions;
import org.springframework.data.mongodb.core.ReactiveMongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Component
@RequiredArgsConstructor
public class ProductPersistenceMongoDb implements ProductPersistence {

    private final ProductRepository productRepository;
    private final ReactiveMongoTemplate reactiveMongoTemplate;
    private final MockStockService mockStockService;


    @Override
    public Flux<Product> saveAll(Flux<Product> products) {
        Flux<ProductDocument> productDocumentFlux = products
                .map(ProductDocument::new);

        return this.productRepository.saveAll(productDocumentFlux)
                .map(ProductDocument::toProduct);
    }

    @Override
    public Flux<Product> findAll() {
        return this.productRepository.findAll()
                .map(ProductDocument::toProduct);
    }

    @Override
    public Mono<Product> updateStock(String cod, Integer stock) {
        Query query = new Query(Criteria.where("cod").is(cod));
        Update update = new Update().set("stock", stock);

        return this.reactiveMongoTemplate
                .update(ProductDocument.class)
                .matching(query)
                .apply(update)
                .withOptions(FindAndModifyOptions.options().returnNew(true))
                .findAndModify()
                .map(ProductDocument::toProduct);
    }

    @Override
    public Mono<Product> readAndWriteStockByCod(String cod, Integer stockIncrement) {
        return this.productRepository.findByCod(cod)
                .switchIfEmpty(Mono.error(new NotFoundException("Product cod: " + cod)))
                .flatMap(article -> {
                    int missingStock = Math.abs(article.getStock() + stockIncrement);
                    if (missingStock > 10) {
                        Mono.error(new ConflictException("Unidades no disponibles (> 10)"));
                    } else if (missingStock > 5) {
                        //TODO stock-10
                    } else if (missingStock >= 0) {
                        //TODO stock 5 Mono defer
                    } else {
                    }

                    article.setStock(article.getStock() + stockIncrement);
                    return this.productRepository.save(article);
                })
                .map(ProductDocument::toProduct);
    }
}
