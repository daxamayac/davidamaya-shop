package com.daxamayac.shop.infrastructure.mongodb.repository;

import com.daxamayac.shop.infrastructure.mongodb.documents.OrderDocument;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;

public interface OrderRepository extends ReactiveCrudRepository<OrderDocument, String> {

}
