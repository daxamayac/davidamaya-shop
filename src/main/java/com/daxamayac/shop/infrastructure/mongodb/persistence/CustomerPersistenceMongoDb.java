package com.daxamayac.shop.infrastructure.mongodb.persistence;

import com.daxamayac.shop.domain.exceptions.ConflictException;
import com.daxamayac.shop.domain.exceptions.NotFoundException;
import com.daxamayac.shop.domain.model.Customer;
import com.daxamayac.shop.domain.persistence.CustomerPersistence;
import com.daxamayac.shop.infrastructure.mongodb.documents.CustomerDocument;
import com.daxamayac.shop.infrastructure.mongodb.repository.CustomerRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Component
@RequiredArgsConstructor
public class CustomerPersistenceMongoDb implements CustomerPersistence {

    private final CustomerRepository customerRepository;

    @Override
    public Mono<Customer> create(Customer customer) {
        CustomerDocument customerDocument = new CustomerDocument(customer);
        return this.customerRepository.save(customerDocument)
                .map(CustomerDocument::toCustomer);
    }

    @Override
    public Flux<Customer> findAll() {
        return this.customerRepository.findAll()
                .map(CustomerDocument::toCustomer);
    }

    @Override
    public Mono<Void> delete(String uuid) {
        return this.customerRepository.deleteByUuid(uuid);
    }

    @Override
    public Mono<Customer> update(String uuid, Customer customer) {

        Mono<CustomerDocument> customerDocument;
        if (!uuid.equals(customer.getUuid())) {
            customerDocument = this.assertUuidNotExist(customer.getUuid())
                    .then(this.customerRepository.findByUuid(uuid));
        } else {
            customerDocument = this.customerRepository.findByUuid(uuid);
        }

        return customerDocument
                .switchIfEmpty(Mono.error(new NotFoundException("Customer uuid: " + uuid)))
                .map(actualCustomer -> {
                    BeanUtils.copyProperties(customer, actualCustomer);
                    return actualCustomer;
                })
                .flatMap(this.customerRepository::save)
                .map(CustomerDocument::toCustomer);
    }

    @Override
    public Mono<Void> assertCustomerExist(String uuid) {
        return this.customerRepository.existsByUuid(uuid)
                .flatMap(existsEmail -> {
                    if (Boolean.FALSE.equals(existsEmail)) {
                        return Mono.error(new ConflictException("Customer not exists cod:" + uuid));
                    }
                    return Mono.empty();
                });
    }

    private Mono<Void> assertUuidNotExist(String uuid) {
        return this.customerRepository.findByUuid(uuid)
                .flatMap(tagEntity -> Mono.error(
                        new ConflictException("Customer uuid already exists : " + uuid)
                ));
    }
}
