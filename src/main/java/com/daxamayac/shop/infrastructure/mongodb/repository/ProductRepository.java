package com.daxamayac.shop.infrastructure.mongodb.repository;

import com.daxamayac.shop.infrastructure.mongodb.documents.ProductDocument;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import reactor.core.publisher.Mono;

public interface ProductRepository extends ReactiveCrudRepository<ProductDocument, String> {

    Mono<ProductDocument> findByCod(String cod);
}
