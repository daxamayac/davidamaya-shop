package com.daxamayac.shop.infrastructure.mongodb.repository;

import com.daxamayac.shop.infrastructure.mongodb.documents.ShopDocument;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import reactor.core.publisher.Mono;

public interface ShopRepository extends ReactiveCrudRepository<ShopDocument, String> {

    Mono<ShopDocument> findByCod(String cod);
}
