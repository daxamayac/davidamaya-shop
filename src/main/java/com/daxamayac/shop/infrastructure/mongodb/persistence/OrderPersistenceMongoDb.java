package com.daxamayac.shop.infrastructure.mongodb.persistence;

import com.daxamayac.shop.domain.model.Order;
import com.daxamayac.shop.domain.persistence.OrderPersistence;
import com.daxamayac.shop.domain.persistence.ProductPersistence;
import com.daxamayac.shop.infrastructure.mongodb.documents.OrderDocument;
import com.daxamayac.shop.infrastructure.mongodb.repository.OrderRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.stream.Stream;

@Component
@RequiredArgsConstructor
public class OrderPersistenceMongoDb implements OrderPersistence {

    private final OrderRepository orderRepository;
    private final ProductPersistence productPersistence;


    @Override
    public Mono<Order> create(Order order) {
        OrderDocument orderDocument = new OrderDocument(order);
        
        return Flux.fromStream(order.getOrderLines() == null ?
                        Stream.empty() :
                        order.getOrderLines().stream())
                .flatMap(orderLine ->
                        this.productPersistence.readAndWriteStockByCod(orderLine.getProductCod(), -orderLine.getAmount())
                )
                .then(Mono.just(orderDocument))
                .flatMap(this.orderRepository::save)
                .map(OrderDocument::toOrder);

    }
}