package com.daxamayac.shop.infrastructure.mongodb.documents;

import com.daxamayac.shop.domain.model.Product;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.beans.BeanUtils;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import java.math.BigDecimal;

@Data
@NoArgsConstructor
@Document
public class ProductDocument {
    @Id
    private String id;
    @Indexed(unique = true)
    private String cod;
    private String name;
    private BigDecimal price;
    private Integer stock;

    public ProductDocument(Product product) {
        BeanUtils.copyProperties(product, this);
    }


    public Product toProduct() {
        Product product = new Product();
        BeanUtils.copyProperties(this, product);
        return product;
    }
}
