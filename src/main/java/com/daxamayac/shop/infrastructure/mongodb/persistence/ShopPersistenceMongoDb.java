package com.daxamayac.shop.infrastructure.mongodb.persistence;

import com.daxamayac.shop.domain.exceptions.ConflictException;
import com.daxamayac.shop.domain.exceptions.NotFoundException;
import com.daxamayac.shop.domain.model.OrderLine;
import com.daxamayac.shop.domain.model.Shop;
import com.daxamayac.shop.domain.persistence.ShopPersistence;
import com.daxamayac.shop.infrastructure.mongodb.documents.ShopDocument;
import com.daxamayac.shop.infrastructure.mongodb.repository.ProductRepository;
import com.daxamayac.shop.infrastructure.mongodb.repository.ShopRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Component
@RequiredArgsConstructor
public class ShopPersistenceMongoDb implements ShopPersistence {

    private final ShopRepository shopRepository;
    private final ProductRepository productRepository;


    @Override
    public Mono<Shop> addProduct(String cod, String productCod) {
        Mono<ShopDocument> shopDocument = this.shopRepository.findByCod(cod);

        return shopDocument
                .switchIfEmpty(Mono.error(new NotFoundException("Shop cod: " + cod)))
                .flatMap(shopDocument1 -> {
                    if (Objects.isNull(shopDocument1.getProducts())) {
                        shopDocument1.setProducts(new ArrayList<>());
                    }
                    return this.productRepository.findByCod(productCod)
                            .switchIfEmpty(Mono.error(new NotFoundException("Product cod: " + productCod)))
                            .map(productDocument -> {
                                shopDocument1.addProduct(productDocument);
                                return shopDocument1;
                            });
                })
                .flatMap(this.shopRepository::save)
                .map(ShopDocument::toShop);
    }

    @Override
    public Mono<Shop> removeProduct(String cod, String productCod) {
        Mono<ShopDocument> shopDocument = this.shopRepository.findByCod(cod);

        return shopDocument
                .switchIfEmpty(Mono.error(new NotFoundException("Shop cod: " + cod)))
                .flatMap(shopDocument1 -> {
                    if (Objects.isNull(shopDocument1.getProducts())) {
                        shopDocument1.setProducts(new ArrayList<>());
                    }
                    return this.productRepository.findByCod(productCod)
                            .switchIfEmpty(Mono.error(new NotFoundException("Product cod: " + productCod)))
                            .map(productDocument -> {
                                shopDocument1.removeProduct(productDocument);
                                return shopDocument1;
                            });
                })
                .flatMap(this.shopRepository::save)
                .map(ShopDocument::toShop);
    }

    @Override
    public Mono<Void> assertShopAndProductExists(List<OrderLine> orderLines) {

        return Flux.fromStream(orderLines.stream())
                .flatMap(orderLine -> this.shopRepository.findByCod(orderLine.getShopCod())
                        .switchIfEmpty(Mono.error(new ConflictException("Shop cod: " + orderLine.getShopCod())))
                        .flatMap(shopDocument -> {
                            boolean existsProductInShop = shopDocument.getProducts()
                                    .stream()
                                    .anyMatch(productDocument -> productDocument.getCod().equals(orderLine.getProductCod()));
                            if (Boolean.FALSE.equals(existsProductInShop))
                                return Mono.error(new ConflictException("Product non exists:" + orderLine.getProductCod() + " in shop " + orderLine.getShopCod()));
                            else
                                return Mono.empty();
                        }))
                .then();
    }
}