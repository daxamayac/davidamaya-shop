package com.daxamayac.shop.infrastructure.mongodb.documents;

import com.daxamayac.shop.domain.model.Shop;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.beans.BeanUtils;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Data
@NoArgsConstructor
@Document
public class ShopDocument {
    @Id
    private String id;
    @Indexed(unique = true)
    private String cod;
    private String name;
    private String address;
    private String email;

    @DBRef(lazy = true)
    private List<ProductDocument> products;

    public ShopDocument(Shop shop) {
        BeanUtils.copyProperties(shop, this);
        this.products = new ArrayList<>();
    }

    public void addProduct(ProductDocument productDocument) {
        this.products.add(productDocument);
    }

    public void removeProduct(ProductDocument productDocument) {
        this.products.remove(productDocument);
    }

    public Shop toShop() {
        Shop shop = new Shop();
        BeanUtils.copyProperties(this, shop);

        shop.setProducts(this.getProducts().stream()
                .map(ProductDocument::toProduct)
                .collect(Collectors.toList())
        );
        return shop;
    }
}
