package com.daxamayac.shop.application.rest.endpoints;

import com.daxamayac.shop.domain.model.Product;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.web.reactive.server.WebTestClient;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

@RestTestConfig
class ProductEndpointTest {

    @Autowired
    WebTestClient webTestClient;


    @Test
    void findAll() {
        this.webTestClient
                .get()
                .uri(uriBuilder -> uriBuilder
                        .path(ProductEndpoint.PRODUCTS)
                        .queryParam("fields", "cod,name")
                        .build())
                .exchange()
                .expectBodyList(Product.class)
                .value(products -> {
                            List<Product> prod1List = products.stream().filter(product -> product.getCod().equals("prod-1")).toList();
                            assertEquals(1, prod1List.size());

                            Product prod1 = prod1List.get(0);
                            assertEquals("prod-1", prod1.getCod());
                            assertEquals("prod-name-1", prod1.getName());
                            assertNull(prod1.getId());
                            assertNull(prod1.getPrice());
                            assertNull(prod1.getStock());
                        }
                );
    }

    @Test
    void updateStock() {
        this.webTestClient
                .patch()
                .uri(ProductEndpoint.PRODUCTS + ProductEndpoint.COD, "prod-1")
                .bodyValue(99)
                .exchange()
                .expectBody(Product.class)
                .value(product -> {
                            assertEquals(99, product.getStock());
                            assertEquals("prod-1", product.getCod());
                        }
                );
    }

}
