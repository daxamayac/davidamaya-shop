package com.daxamayac.shop.application.rest.endpoints;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.web.reactive.server.WebTestClient;

import static org.junit.jupiter.api.Assertions.assertEquals;

@RestTestConfig
class ProductEndpointErrorTest {

    @Autowired
    WebTestClient webTestClient;

    @Test
    void updateStockErrorNewStockIsZero() {
        this.webTestClient
                .patch()
                .uri(ProductEndpoint.PRODUCTS + ProductEndpoint.COD, "prod-1")
                .bodyValue(0)
                .exchange()
                .expectStatus().is4xxClientError()
                .expectBody(String.class)
                .value(s -> {
                    String message = this.readMessageFromJson(s);
                    assertEquals("Bad Request Exception. Stock has to be greater than zero", message);
                });
    }

    @Test
    void updateStockErrorNewStockIsNegative() {
        this.webTestClient
                .patch()
                .uri(ProductEndpoint.PRODUCTS + ProductEndpoint.COD, "prod-1")
                .bodyValue(-99)
                .exchange()
                .expectStatus().is4xxClientError()
                .expectBody(String.class)
                .value(s -> {
                    String message = this.readMessageFromJson(s);
                    assertEquals("Bad Request Exception. Stock has to be greater than zero", message);
                });
    }

    private String readMessageFromJson(String json) {
        String resp = null;
        try {
            ObjectMapper mapper = new ObjectMapper();
            JsonNode actualObj;
            actualObj = mapper.readTree(json);
            resp = actualObj.get("message").textValue();
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return resp;
    }

}
