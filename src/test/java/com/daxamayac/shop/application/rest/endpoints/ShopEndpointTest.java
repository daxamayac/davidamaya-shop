package com.daxamayac.shop.application.rest.endpoints;

import com.daxamayac.shop.domain.model.Shop;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.web.reactive.server.WebTestClient;
import reactor.core.publisher.Mono;

import static org.junit.jupiter.api.Assertions.*;

@RestTestConfig
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class ShopEndpointTest {

    @Autowired
    WebTestClient webTestClient;

    @BeforeAll
    void init() {
        this.webTestClient
                .patch()
                .uri(ShopEndpoint.SHOPS + ShopEndpoint.COD + ShopEndpoint.PRODUCTS + ShopEndpoint.ADD, "shop-1")
                .body(Mono.just("prod-2"), String.class)
                .exchange()
                .expectBody(Shop.class)
                .value(shop -> {
                            assertEquals("shop-1", shop.getCod());
                            assertTrue(shop.getProducts().stream().anyMatch(product -> product.getCod().equals("prod-2")));
                        }
                );
    }


    @Test
    void addProduct() {
        this.webTestClient
                .patch()
                .uri(ShopEndpoint.SHOPS + ShopEndpoint.COD + ShopEndpoint.PRODUCTS + ShopEndpoint.ADD, "shop-1")
                .body(Mono.just("prod-1"), String.class)
                .exchange()
                .expectBody(Shop.class)
                .value(shop -> {
                            assertEquals("shop-1", shop.getCod());
                            assertTrue(shop.getProducts().stream().anyMatch(product -> product.getCod().equals("prod-1")));
                        }
                );
    }

    @Test
    void removeProduct() {
        this.webTestClient
                .patch()
                .uri(ShopEndpoint.SHOPS + ShopEndpoint.COD + ShopEndpoint.PRODUCTS + ShopEndpoint.REMOVE, "shop-1")
                .body(Mono.just("prod-2"), String.class)
                .exchange()
                .expectBody(Shop.class)
                .value(shop -> {
                            assertEquals("shop-1", shop.getCod());
                            assertFalse(shop.getProducts().stream().anyMatch(product -> product.getCod().equals("prod-2")));
                        }
                );
    }

}