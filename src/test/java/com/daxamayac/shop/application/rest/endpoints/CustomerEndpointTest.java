package com.daxamayac.shop.application.rest.endpoints;

import com.daxamayac.shop.domain.model.Customer;
import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.web.reactive.server.WebTestClient;
import reactor.core.publisher.Mono;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

@RestTestConfig
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class CustomerEndpointTest {

    @Autowired
    WebTestClient webTestClient;

    @BeforeAll
    void init() {
        Customer customer = Customer.builder()
                .name("David Amaya")
                .uuid("forDelete")
                .build();

        this.webTestClient
                .post()
                .uri(CustomerEndpoint.CUSTOMERS)
                .body(Mono.just(customer), Customer.class)
                .exchange()
                .expectStatus().isOk()
                .expectBody(Customer.class)
                .value(Assertions::assertNotNull)
                .value(customer1 -> {
                    assertEquals("David Amaya", customer1.getName());
                    assertEquals("forDelete", customer1.getUuid());
                });
    }

    @Test
    @Order(1)
    void create() {
        Customer customer = Customer.builder()
                .name("David Amaya")
                .uuid("daxamayac")
                .build();

        this.webTestClient
                .post()
                .uri(CustomerEndpoint.CUSTOMERS)
                .body(Mono.just(customer), Customer.class)
                .exchange()
                .expectStatus().isOk()
                .expectBody(Customer.class)
                .value(Assertions::assertNotNull)
                .value(customer1 -> {
                    assertEquals("David Amaya", customer1.getName());
                    assertEquals("daxamayac", customer1.getUuid());
                });
    }

    @Test
    void findAll() {
        this.webTestClient
                .get()
                .uri(CustomerEndpoint.CUSTOMERS)
                .exchange()
                .expectBodyList(Customer.class)
                .value(customers -> {
                            List<Customer> customerList = customers.stream()
                                    .filter(customer -> customer.getUuid().equals("daxamayac")).toList();
                            assertEquals(1, customerList.size());

                            Customer customer = customerList.get(0);
                            assertEquals("David Amaya", customer.getName());

                        }
                );
    }

    @Test
    void delete() {
        this.webTestClient
                .delete()
                .uri(CustomerEndpoint.CUSTOMERS + CustomerEndpoint.UUID, "forDelete")
                .exchange()
                .expectStatus().isOk();
    }

    @Test
    void update() {
        Customer customer = Customer.builder()
                .uuid("daxamayac")
                .name("David Amaya Cruz")
                .build();
        this.webTestClient
                .put()
                .uri(CustomerEndpoint.CUSTOMERS + CustomerEndpoint.UUID, "daxamayac")
                .body(Mono.just(customer), Customer.class)
                .exchange()
                .expectBody(Customer.class)
                .value(customer1 -> {
                    assertEquals("daxamayac", customer1.getUuid());
                    assertEquals("David Amaya Cruz", customer1.getName());
                });
    }
}
