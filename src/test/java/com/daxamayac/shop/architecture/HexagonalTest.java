package com.daxamayac.shop.architecture;

import com.tngtech.archunit.core.importer.ImportOption;
import com.tngtech.archunit.junit.AnalyzeClasses;
import com.tngtech.archunit.junit.ArchTest;
import com.tngtech.archunit.lang.ArchRule;

import static com.tngtech.archunit.library.Architectures.onionArchitecture;

@AnalyzeClasses(packages = "com.daxamayac", importOptions = {ImportOption.DoNotIncludeTests.class})
public class HexagonalTest {

    @ArchTest
    static final ArchRule archUnitOnion = onionArchitecture()
            .domainModels("..domain.model..")
            .domainServices("..domain.services..", "..domain.persistence..", "..domain.mock..")
            .applicationServices("..infrastructure.configuration..")
            .adapter("rest", "..application.rest..")
            .adapter("persistence", "..infrastructure.mongodb..")
            .adapter("mock", "..infrastructure.mock..");

}
