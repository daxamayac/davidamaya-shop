package com.daxamayac.shop.architecture;

import com.tngtech.archunit.junit.AnalyzeClasses;
import com.tngtech.archunit.junit.ArchTest;
import com.tngtech.archunit.lang.ArchRule;

import static com.tngtech.archunit.lang.syntax.ArchRuleDefinition.classes;

@AnalyzeClasses(packages = "com.daxamayac.shop")
public class PackageDependencyTest {

    @ArchTest
    static final ArchRule independentDomainModelPackage = classes()
            .that().resideInAPackage("..domain.model..")
            .should().dependOnClassesThat().resideInAnyPackage("..domain.model..", "java..");

    @ArchTest
    static final ArchRule independentDomainPackage = classes()
            .that().resideInAPackage("..domain..")
            .should().dependOnClassesThat().resideInAnyPackage("..domain..", "java..");

    @ArchTest
    static final ArchRule restDependPackage = classes()
            .that().resideInAPackage("..application.rest..")
            .should().dependOnClassesThat().resideInAnyPackage("..domain.model..", "..domain.service..", "java..", "reactor.core.publisher..");

    @ArchTest
    static final ArchRule infrastructureRepositoryMongoDependPackage = classes()
            .that().resideInAPackage("..infrastructure.mongodb..")
            .should().dependOnClassesThat().resideInAnyPackage("..domain.model..", "java..", "reactor.core.publisher..");

}
