package com.daxamayac.shop.architecture;

import com.tngtech.archunit.junit.AnalyzeClasses;
import com.tngtech.archunit.junit.ArchTest;
import com.tngtech.archunit.lang.ArchRule;

import static com.tngtech.archunit.lang.syntax.ArchRuleDefinition.classes;

@AnalyzeClasses(packages = "com.daxamayac.shop")
public class InheritanceTest {

    @ArchTest
    static final ArchRule implementsPersistenceResideOnlyInfrastructurePersistence = classes()
            .that().implement("..domain.persistence..")
            .should().resideInAPackage("..domain.infrastructure.persistence");

}
