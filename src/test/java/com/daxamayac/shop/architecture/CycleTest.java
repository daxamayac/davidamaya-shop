package com.daxamayac.shop.architecture;

import com.tngtech.archunit.core.importer.ImportOption;
import com.tngtech.archunit.junit.AnalyzeClasses;
import com.tngtech.archunit.junit.ArchTest;
import com.tngtech.archunit.lang.ArchRule;

import static com.tngtech.archunit.library.dependencies.SlicesRuleDefinition.slices;

@AnalyzeClasses(packages = "com.daxamayac.shop", importOptions = {ImportOption.DoNotIncludeTests.class})
public class CycleTest {

    @ArchTest
    static ArchRule generalCycleTest =
            slices().matching("com.daxamayac.shop.(*)..")
                    .should().beFreeOfCycles();


}
