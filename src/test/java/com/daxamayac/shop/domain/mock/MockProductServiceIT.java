package com.daxamayac.shop.domain.mock;

import com.daxamayac.shop.TestConfig;
import com.daxamayac.shop.domain.model.Product;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import reactor.test.StepVerifier;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertTrue;


@TestConfig
class MockProductServiceIT {

    @Autowired
    private MockProductService mockProductService;

    @Test
    void findAllProducts() {
        StepVerifier.create(this.mockProductService.findAllProducts())
                .recordWith(ArrayList::new)
                .thenConsumeWhile(it -> true)
                .expectRecordedMatches(products -> {
                    List<String> cods = products.stream().map(Product::getCod).toList();
                    assertTrue(cods.containsAll(List.of("prod-1", "prod-2", "prod-3", "prod-4", "prod-5", "prod-6")));
                    return true;
                })
                .verifyComplete();
    }


}
